!! Ne pas changer le formattage !!
# 1 Commande Linux

## 1.1 Commandes fréquentes
1- i 
2- x
3- o
4- c
5- j
6- v
7- y
8- d
9- a
10- n
11- p
12- k
13- m
14- g
15- w
16- l
17- t
18- r
19- e
20- q
21- u
22- b
23- s
24- h
25- f

## 1.2 Commandes ls
1- 0
2- 8
3- vader
4- def-dooku

## 1.3 Permissions
1- Non
2- Oui
3-
  a- faux
  b- faux
  c- vrai
4- chmod g-w toto.txt 

## 1.4 Flux de sortie
1- 
  a- 
  b- 
2- 

## 1.5 Opérateur de chaînage (|)
1- cat /proc/cpuinfo
2- cat /proc/cpuinfo | grep name
3-
commande-
cat /usr/include/math.h | grep M_E
sortie-
# define M_E		2.7182818284590452354	/* e */
# define M_El		2.718281828459045235360287471352662498L /* e */
# define M_Ef16		__f16 (2.718281828459045235360287471352662498) /* e */
# define M_Ef32		__f32 (2.718281828459045235360287471352662498) /* e */
# define M_Ef64		__f64 (2.718281828459045235360287471352662498) /* e */
# define M_Ef128	__f128 (2.718281828459045235360287471352662498) /* e */
# define M_Ef32x	__f32x (2.718281828459045235360287471352662498) /* e */
# define M_Ef64x	__f64x (2.718281828459045235360287471352662498) /* e */

4- 
commande- ls /usr/bin | wc
nombre de fichier- 1545

## 1.6 Gestion des tâches 
1- d
2- Elle n'arrêtera jamais
3- Ça tue le processus
4- Ça suspend le processus (sans le tuer)
5- kill %2
6- bg
7- fg
8- faux

## 1.7 Variables d'environnement
1- env | grep PATH
2- export PATH="x/z/y:$PATH"

# 2 Compilation et exécution
## 2.2 Compilation
1- Compiler et assembler
2- gcc qui-suis-je.c -o qui-suis-je
3-
Sortie- Utilisateur: 1000
Processus: 26573
Processus parent: 28686
- Nom: Équipe 43
<début à remplacer ici>

<fin à remplacer ici>

## 2.3 Appels système
1- execve: exécuter un programme
2- write

## 2.4 Débogage
1- gcc -g -o qui-suis-je-debug qui-suis-je.c
2- gdb qui-suis-je-debug
3- break 6
4- run
5-
commande- bt
sortie- #- main() at qui-suis-je.c:6
<début à remplacer ici>

<fin à remplacer ici>

## 2.5
2- gdb ./plante core
3-
sortie-#0  __GI_raise (sig=sig@entry=6) at ../sysdeps/unix/sysv/linux/raise.c:51
51	in ../sysdeps/unix/sysv/linux/raise.c

# 4 Programmation avec threads
1- 0.001193 secondes
2- 0.005196 secondes
Le résultat ne correspond pas à nos attentes, puisqu'il est plus élevé que le temps d'exécution du programme avec un seul core. Une des raison pourquoi le temps d'exécution est plus élevé lors de l'utilisation de plusieurs core pourrait être le temps requis pour séparer le travail en 6. Pour des petites tâches, le temps de séparation est non négligeable et ralentit le programme.
3-
1 core: 0.012466 secondes
6 cores: 0.009024 secondes
Les résultats correspondent plus à nos attentes. En effet, plus on augmente la taille des matrices, plus le programme utilisant 6 cores est rapide par rapport au programme n'utilisant qu'un seul core.
4- Suite à nos tests, nous en concluons qu'il ne vaut pas toujours la peine d'utiliser plusieurs threads lorsque nous avons une tâche à faire. Si la tâche est relativement courte, l'utilisation d'un nombre accru threads ne fait pas ou très peu de différence en ce qui concerne le temps d'exécution d'un programme. Cependant, si le programme doit effectuer un grand nombre de calculs, il peut être très intéressant d'utiliser plusieurs threads.

#### Fin du rapport ####
