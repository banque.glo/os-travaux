#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>


void skywalker() {
    int child_process_id = fork();
    if (child_process_id) {
        printf("Non! Je suis ton père. Ton numéro de processus est %d.\n", child_process_id);
        wait(child_process_id);
    } else {
        printf("Nooon! Ce n'est pas vrai! Je suis le processus %d.\n", getpid());
        int dodoTime = 2*60 + 11;
        sleep(dodoTime);
        kill(getppid());
    }
}

int main(int argc, char *argv[]) {
    skywalker();
}

