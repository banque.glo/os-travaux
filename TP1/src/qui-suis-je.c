#include <unistd.h>
#include <stdio.h>

int main() {
    printf("Utilisateur: %d\n", getuid());
    printf("Processus: %d\n", getpid());
    printf("Processus parent: %d\n", getppid());
    printf("- Nom: Équipe 43");

    return 0;
}