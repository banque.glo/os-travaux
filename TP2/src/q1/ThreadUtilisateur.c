/**************************************************************************
    Travail pratique No 2 : Thread utilisateurs
    
    Ce fichier est votre implémentation de la librarie des threads utilisateurs.
         
	Systemes d'explotation GLO-2001
	Universite Laval, Quebec, Qc, Canada.
(c) 2020 Antoine Lefrancois
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ucontext.h>
#include <time.h>
#include <signal.h>
#include <sys/time.h>
#include "ThreadUtilisateur.h"

/* Définitions privées, donc pas dans le .h, car l'utilisateur n'a pas besoin de
   savoir ces détails d'implémentation. OBLIGATOIRE. */
typedef enum { 
	THREAD_EXECUTE=0,
	THREAD_PRET,
	THREAD_BLOQUE,
	THREAD_TERMINE
} EtatThread;

#define TAILLE_PILE 8192   // Taille de la pile utilisée pour les threads

/* Structure de données pour créer une liste chaînée simple sur les threads qui ont fait un join.
   Facultatif */
typedef struct WaitList {
	struct TCB *pThreadWaiting;
	struct WaitList *pNext;
} WaitList;

/* TCB : Thread Control Block. Cette structure de données est utilisée pour stocker l'information
   pour un thread. Elle permet aussi d'implémenter une liste doublement chaînée de TCB, ce qui
   facilite la gestion et permet de faire un ordonnanceur tourniquet sans grand effort.  */
typedef struct TCB {  // Important d'avoir le nom TCB ici, sinon le compilateur se plaint.
	tid                 id;        // Numero du thread
	EtatThread			etat;      // Etat du thread
	ucontext_t          ctx;       // Endroit où stocker le contexte du thread
	time_t              WakeupTime; // Instant quand réveiller le thread, s'il dort, en epoch time.
	struct TCB         *pSuivant;   // Liste doublement chaînée, pour faire un buffer circulaire
	struct TCB         *pPrecedant; // Liste doublement chaînée, pour faire un buffer circulaire
	struct WaitList	   *pWaitListJoinedThreads; // Liste chaînée simple des threads en attente.
} TCB;

// Pour que les variables soient absolument cachées à l'utilisateur, on va les déclarer static
static TCB *gpThreadCourant = NULL;	 // Thread en cours d'execution
static TCB *gpNextToExecuteInCircularBuffer = NULL;
static int gNumberOfThreadInCircularBuffer = 0;
static int gNextThreadIDToAllocate = 0;
static WaitList *gpWaitTimerList = NULL; 
static TCB *gThreadTable[MAX_THREADS]; // Utilisé par la fonction ThreadID()

/* Cette fonction ne fait rien d'autre que de spinner un tour et céder sa place. C'est l'équivalent 
   pour un système de se tourner les pouces. */
void IdleThreadFunction(void *arg) {
	struct timespec SleepTime, TimeRemaining;
	SleepTime.tv_sec = 0;
	SleepTime.tv_nsec = 250000000;
	while (1) {
		printf("                #########  Idle Thread 0 s'exécute et va prendre une pose de 250 ms... #######\n");
		/* On va dormir un peu, pour ne pas surcharger inutilement le processus/l'affichage. Dans un
		   vrai système d'exploitation, s'il n'y a pas d'autres threads d'actifs, ce thread demanderait au
		   CPU de faire une pause, car il n'y a rien à faire. */ 		
	    nanosleep(&SleepTime,&TimeRemaining); // nanosleep interfere moins avec les alarmes.
		ThreadCeder();
	}
}

int createIdleThread();
int createMainThread();
char getTcbState(EtatThread);
void displayBufferContent();
void addThreadInCircularBuffer(tid threadID);
void removeThreadFromWaitTimerList();
void yayeet();

/* ****************************************************************************************** 
                                   T h r e a d I n i t
   ******************************************************************************************/
int ThreadInit(void){
	printf("\n  ******************************** ThreadInit()  ******************************** \n");

	if (createIdleThread() < 0) {
		return -1;
	}
	if (createMainThread() < 0) {
		return -1;
	}
    return 1;
}

int createIdleThread() {
	TCB *tcbIdle = (TCB*)malloc(sizeof(TCB));
	if(tcbIdle == NULL) {
		return -1;
	}

	getcontext(&tcbIdle->ctx);
	char* stack = malloc(TAILLE_PILE);
	if (stack == NULL) {
		return -1;
	}
	tcbIdle->ctx.uc_stack.ss_sp = stack;
	tcbIdle->ctx.uc_stack.ss_size = TAILLE_PILE;
	makecontext(&tcbIdle->ctx, (void*)IdleThreadFunction,1, NULL);

	tcbIdle->etat = THREAD_PRET;
	tcbIdle->id = gNextThreadIDToAllocate;
	gNextThreadIDToAllocate++;
	gThreadTable[0] = tcbIdle;
	tcbIdle->pWaitListJoinedThreads = NULL;
	tcbIdle->pSuivant = NULL;
	tcbIdle->pPrecedant = NULL;
	gNumberOfThreadInCircularBuffer++;
	gpNextToExecuteInCircularBuffer = tcbIdle;

	return 1;
}

int createMainThread() {
	TCB *tcbIdle = gThreadTable[0];
	TCB *tcbMain = (TCB*)malloc(sizeof(TCB));
	if (tcbMain == NULL) {
		return -1;
	}
	tcbMain->id = gNextThreadIDToAllocate;
	gNextThreadIDToAllocate++;
	gThreadTable[1] = tcbMain;
	tcbMain->pSuivant = tcbIdle;
	tcbMain->pPrecedant = tcbIdle;
	tcbIdle->pPrecedant = tcbMain;
	tcbIdle->pSuivant = tcbMain;
	gpThreadCourant = tcbMain;
	gNumberOfThreadInCircularBuffer++;
	return 1;
}


/* ****************************************************************************************** 
                                   T h r e a d C r e e r
   ******************************************************************************************/
tid ThreadCreer(void (*pFuncThread)(void *), void *arg) {
	printf("\n  ******************************** ThreadCreer(%p,%p) ******************************** \n",pFuncThread,arg);
	TCB *tcb = (TCB*)malloc(sizeof(TCB));
	if (tcb == NULL) {
		return -1;
	}
	getcontext(&tcb->ctx);

	char *stack = malloc(TAILLE_PILE);
	if (stack == NULL) {
		return -1;
	}
	tcb->ctx.uc_stack.ss_sp = stack;
	tcb->ctx.uc_stack.ss_size = TAILLE_PILE;

	makecontext(&tcb->ctx, (void*)pFuncThread, 1, arg);

	tcb->id = gNextThreadIDToAllocate;
	gNextThreadIDToAllocate++;
	gThreadTable[tcb->id] = tcb;
	addThreadInCircularBuffer(tcb->id);
	tcb->pWaitListJoinedThreads = NULL;

	return (tid) tcb->id;
}

void addThreadInCircularBuffer(tid threadIDtoAdd) {
	TCB *tcbToAdd = gThreadTable[threadIDtoAdd];

	tcbToAdd->pPrecedant = gpNextToExecuteInCircularBuffer->pPrecedant;
	gpNextToExecuteInCircularBuffer->pPrecedant->pSuivant = tcbToAdd;
	tcbToAdd->pSuivant = gpNextToExecuteInCircularBuffer;
	gpNextToExecuteInCircularBuffer->pPrecedant = tcbToAdd;
	gNumberOfThreadInCircularBuffer++;

	tcbToAdd->etat = THREAD_PRET;
	gpNextToExecuteInCircularBuffer = tcbToAdd;
}

/* ****************************************************************************************** 
                                   T h r e a d C e d e r
   ******************************************************************************************/
void ThreadCeder(void){
	printf("\n  ******************************** ThreadCeder()  ******************************** \n");
	printf("----- Etat de l'ordonnanceur avec %d threads -----\n", gNumberOfThreadInCircularBuffer);

	displayBufferContent();
	removeThreadFromWaitTimerList();
	yayeet();


	TCB *oldThreadCourant = gpThreadCourant;
	if (oldThreadCourant->etat == THREAD_EXECUTE) {
		oldThreadCourant->etat = THREAD_PRET;
	}
	gpThreadCourant = gpNextToExecuteInCircularBuffer;
	gpThreadCourant->etat = THREAD_EXECUTE;
	gpNextToExecuteInCircularBuffer = gpNextToExecuteInCircularBuffer->pSuivant;
	swapcontext(&oldThreadCourant->ctx, &gpThreadCourant->ctx);
}

void displayBufferContent() {
	printf("prochain->");
	TCB *tcb = gpNextToExecuteInCircularBuffer;
	for (int i = 0; i < gNumberOfThreadInCircularBuffer; i++) {
		char startSpacing[11];  
		if (i == 0) {
			strcpy(startSpacing, "");
		}
		else {
			strcpy(startSpacing, "          ");
		}
		printf("%sThreadID:%d ", startSpacing, tcb->id);

		char tcbStateLetter = getTcbState(tcb->etat);
		printf("État:%c ", tcbStateLetter);

		char idleThreadIndicator[22] = ""; 
		if (tcb->id == (tid)0) {
			strcpy(idleThreadIndicator, "*Special Idle Thread*");
		}
		printf("%s Waitlist", idleThreadIndicator);

		WaitList *joinedThread = tcb->pWaitListJoinedThreads;
		while(joinedThread != NULL) {
			tid waitingThreadID = joinedThread->pThreadWaiting->id;
			printf("-->(%d)", waitingThreadID);
			joinedThread = joinedThread->pNext;
		}
		printf("\n");
		tcb = tcb->pSuivant;
	}

	printf("----- Liste des threads qui dorment, epoch time=%ld -----\n", time(NULL));
	WaitList *waitList = gpWaitTimerList;
	while(waitList != NULL) {
	
		tid waitingThreadID = waitList->pThreadWaiting->id;
		char startSpacing[11];  
		strcpy(startSpacing, "          ");
		printf("%sThreadID:%d ", startSpacing, waitingThreadID);
		printf("État:%c ", getTcbState(gThreadTable[waitingThreadID]->etat));
		printf("WakeTime=%ld", gThreadTable[waitingThreadID]->WakeupTime);
		char idleThreadIndicator[22] = ""; 
		if (waitingThreadID == (tid)0) {
			strcpy(idleThreadIndicator, "*Special Idle Thread*");
		}
		printf("%s Waitlist", idleThreadIndicator);
		WaitList *joinedThread = waitList->pThreadWaiting->pWaitListJoinedThreads;
		while(joinedThread != NULL) {
			tid waitingThreadID = joinedThread->pThreadWaiting->id;
			printf("-->(%d)", waitingThreadID);
			joinedThread = joinedThread->pNext;
		}
		printf("\n");
		waitList = waitList->pNext;
	}
	printf("--------------------------------------------------\n");
}


char getTcbState(EtatThread etat) {
	if (etat == THREAD_EXECUTE) {
		return 'E';
	}
	else if (etat == THREAD_PRET) {
		return 'P';
	}
	else if (etat == THREAD_BLOQUE) {
		return 'B';
	}
	else if (etat == THREAD_TERMINE) {
		return 'T';
	}
}

void removeThreadFromWaitTimerList() {
	WaitList *prev = NULL;
	WaitList *currentSleepingThreadsHead = gpWaitTimerList;
	for (currentSleepingThreadsHead = gpWaitTimerList; currentSleepingThreadsHead != NULL; prev = currentSleepingThreadsHead, currentSleepingThreadsHead = currentSleepingThreadsHead->pNext) {
		if (currentSleepingThreadsHead->pThreadWaiting->WakeupTime < time(NULL)) {
			if (prev == NULL) {
				gpWaitTimerList = currentSleepingThreadsHead->pNext;
			}
			else {
				prev->pNext = currentSleepingThreadsHead->pNext;
			}
			addThreadInCircularBuffer(currentSleepingThreadsHead->pThreadWaiting->id);
		}
	}
}

void yayeet() {
	while (gpNextToExecuteInCircularBuffer->etat == THREAD_TERMINE) {
		printf("ThreadCeder: Garbage collection sur le thread %d\n",gpNextToExecuteInCircularBuffer->id);

		gpNextToExecuteInCircularBuffer->pPrecedant->pSuivant = gpNextToExecuteInCircularBuffer->pSuivant;
		gpNextToExecuteInCircularBuffer->pSuivant->pPrecedant = gpNextToExecuteInCircularBuffer->pPrecedant;

		TCB *threadToRemove = gpNextToExecuteInCircularBuffer;
		gpNextToExecuteInCircularBuffer = threadToRemove->pSuivant;

		free(threadToRemove->ctx.uc_stack.ss_sp);
		free(threadToRemove->pWaitListJoinedThreads);
		gThreadTable[threadToRemove->id] = NULL;
		free(threadToRemove);

		gNumberOfThreadInCircularBuffer--;
	}
}

/* ****************************************************************************************** 
                                   T h r e a d J o i n d r e
   ******************************************************************************************/
int ThreadJoindre(tid ThreadID){
	printf("\n  ******************************** ThreadJoindre(%d)  ******************************* \n",ThreadID);
	gpThreadCourant->etat = THREAD_BLOQUE;

	TCB *tcbToJoin = gThreadTable[ThreadID];

	WaitList *waitList = (WaitList*)malloc(sizeof(WaitList));
	if (waitList == NULL) {
		return -1;
	}
	waitList->pThreadWaiting = gpThreadCourant;
	waitList->pNext = tcbToJoin->pWaitListJoinedThreads;
	tcbToJoin->pWaitListJoinedThreads = waitList;

	gpThreadCourant->pSuivant->pPrecedant = gpThreadCourant->pPrecedant;
	gpThreadCourant->pPrecedant->pSuivant = gpThreadCourant->pSuivant;
	gpThreadCourant->pSuivant = NULL;
	gpThreadCourant->pPrecedant = NULL;
	gNumberOfThreadInCircularBuffer--;

	ThreadCeder();
	return 1;
}


/* ****************************************************************************************** 
                                   T h r e a d Q u i t t e r
   ******************************************************************************************/
void ThreadQuitter(void){
	printf("  ******************************** ThreadQuitter(%d)  ******************************** \n",gpThreadCourant->id);
	gpThreadCourant->etat = THREAD_TERMINE;


	WaitList *waitList = gpThreadCourant->pWaitListJoinedThreads;
	while(waitList) {
		printf("ThreadQuitter: je reveille le thread %d", waitList->pThreadWaiting->id);
		addThreadInCircularBuffer(waitList->pThreadWaiting->id);
		waitList = waitList->pNext;
	}
	free(gpThreadCourant->pWaitListJoinedThreads);
	gpThreadCourant->pWaitListJoinedThreads = NULL;

	// On passe au thread suivant
	ThreadCeder();
	printf(" ThreadQuitter:Je ne devrais jamais m'exectuer! Si je m'exécute, vous avez un bug!\n");
	return;
}

/* ****************************************************************************************** 
                                   T h r e a d I d
   ******************************************************************************************/
tid ThreadId(void) {
	// Libre à vous de la modifier. Mais c'est ce que j'ai fait dans mon code, en toute simplicité.
	return gpThreadCourant->id;
}

/* ****************************************************************************************** 
                                   T h r e a d D o r m i r
   ******************************************************************************************/
void ThreadDormir(int secondes) {
	printf("\n  ******************************** ThreadDormir(%d)  ******************************** \n",secondes);
	gpThreadCourant->etat = THREAD_BLOQUE;
	gpThreadCourant->WakeupTime = time(NULL) + secondes;

	gpThreadCourant->pPrecedant->pSuivant = gpThreadCourant->pSuivant;
	gpThreadCourant->pSuivant->pPrecedant = gpThreadCourant->pPrecedant;
	gpThreadCourant->pSuivant = NULL;
	gpThreadCourant->pPrecedant = NULL;
	gNumberOfThreadInCircularBuffer--;

	WaitList *waitList = (WaitList*)malloc(sizeof(WaitList));
	waitList->pThreadWaiting = gpThreadCourant;
	waitList->pNext = gpWaitTimerList;
	gpWaitTimerList = waitList;

	ThreadCeder();
}

