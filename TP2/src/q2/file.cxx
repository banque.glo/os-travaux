
#include "file.h"
#include <stdio.h>

File::File()
{
    execution_terminee = false;
}

void File::Insere(const ItemFile &item)
{
    // work starting
    pthread_mutex_lock(&mutex_stockage);

    while (stockage.size() == TAILLE_STOCKAGE)
    {
        // block until size < taille max
        pthread_cond_wait(&condition_consommateurs, &mutex_stockage);
    }

    stockage.push_back(item);

    // work done
    pthread_mutex_unlock(&mutex_stockage);
    pthread_cond_signal(&condition_producteurs);
}

bool File::Retire(ItemFile &item)
{
    // work starting
    pthread_mutex_lock(&mutex_stockage);

    if (stockage.size() < 1)
    {
        if (execution_terminee && stockage.size() == 0)
        {
            pthread_mutex_unlock(&mutex_stockage);
            return false;
        }
        // block until size > 0
        pthread_cond_wait(&condition_producteurs, &mutex_stockage);
    }
    if (execution_terminee && stockage.size() == 0)
    {
        pthread_mutex_unlock(&mutex_stockage);
        return false;
    }

    item = stockage.front();
    stockage.pop_front();

    // work done
    pthread_mutex_unlock(&mutex_stockage);
    pthread_cond_signal(&condition_consommateurs);
    return true;
}

void File::Termine()
{
    // all work done
    execution_terminee = true;
    pthread_cond_broadcast(&condition_producteurs);
}
