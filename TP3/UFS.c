#include "UFS.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "disque.h"

// Retourne le minimum de deux valeurs
int min(int a, int b)
{
	return a < b ? a : b;
}

// Retourne le maximum de deux valeurs
int max(int a, int b)
{
	return a > b ? a : b;
}

/* Cette fonction va extraire le repertoire d'une chemin d'acces complet.
   Par exemple, si le chemin fourni pPath="/doc/tmp/a.txt", cette fonction va
   copier dans pDir le string "/doc/tmp" . Si le chemin fourni est /a.txt, la fonction
   va retourner / .
*/
int GetDirFromPath(const char *pPath, char *pDir)
{
	strcpy(pDir, pPath);
	int len = strlen(pDir); // length, EXCLUDING null
	int index;

	// On va a reculons, de la fin au debut
	while (pDir[len] != '/')
	{
		len--;
		if (len < 0)
		{
			// Il n'y avait pas de slash dans le pathname
			return 0;
		}
	}
	if (len == 0)
	{
		// Le fichier se trouve dans le root!
		pDir[0] = '/';
		pDir[1] = 0;
	}
	else
	{
		// On remplace le slash par une fin de chaine de caractere
		pDir[len] = '\0';
	}
	return 1;
}

/* Cette fonction va extraire le nom de fichier d'une chemin d'acces complet.
   Par exemple, si le chemin fourni pPath="/doc/tmp/a.txt", cette fonction va
   copier dans pFilename le string "a.txt" . La fonction retourne 1 si elle
   a trouvee le nom de fichier avec succes, et 0 autrement.
*/
int GetFilenameFromPath(const char *pPath, char *pFilename)
{
	// Pour extraire le nom de fichier d'un path complet
	char *pStrippedFilename = strrchr(pPath, '/');
	if (pStrippedFilename != NULL)
	{
		++pStrippedFilename; // On avance pour passer le slash
		if ((*pStrippedFilename) != '\0')
		{
			// On copie le nom de fichier trouve
			strcpy(pFilename, pStrippedFilename);
			return 1;
		}
	}
	return 0;
}

// Exemple de fonction qui marque qu'un bloc de donnee est libere
int ReleaseFreeBlock(UINT16 BlockNum)
{
	char BlockFreeMap[BLOCK_SIZE];
	ReadBlock(FREE_BLOCK_BITMAP, BlockFreeMap);
	BlockFreeMap[BlockNum] = 1;
	printf("Relache block %d\n", BlockNum);
	WriteBlock(FREE_BLOCK_BITMAP, BlockFreeMap);
	return 1;
}
// Votre code a partir d'ici...

int getNumberOfFilesInPath(const char *path)
{
	char pathCopy[sizeof(path)];
	strcpy(pathCopy, path);
	int numberOfFiles = 0;
	char *files = strtok(pathCopy, "/");
	while (files)
	{
		numberOfFiles++;
		files = strtok(NULL, "/");
	}
	return numberOfFiles;
}

int getDirBlockNumber(int dirInodeNumber)
{
	//Les inodes commencent a l'octer 2048
	//Un inode par bloc, donc un inode fait 512 octets
	int dirBlockNumber = ((BLOCK_SIZE * BASE_BLOCK_INODE) + (dirInodeNumber * BLOCK_SIZE)) / BLOCK_SIZE;
	return dirBlockNumber;
}

//Code inspiré de l'énoncé page 6
int getFileInodeNumber(const char *name, int dirParentInodeNumber)
{
	int dirBlockNumber = getDirBlockNumber(dirParentInodeNumber);
	char blockBuffer[BLOCK_SIZE];
	ReadBlock(dirBlockNumber, blockBuffer);
	iNodeEntry *inodeEntry = (iNodeEntry *)blockBuffer;
	int numberOfDirEntry = inodeEntry->iNodeStat.st_size / sizeof(DirEntry);
	ReadBlock(inodeEntry->Block[0], blockBuffer);
	DirEntry *dirEntry = (DirEntry *)blockBuffer;
	for (int i = 0; i < numberOfDirEntry; i++)
	{
		if (strcmp(name, dirEntry[i].Filename) == 0)
		{
			return dirEntry[i].iNode;
		}
	}
	return -1;
}

int getInodeNumberFromPath(const char *path, int inodeNumber)
{
	if (strcmp(path, "/") == 0)
	{
		return ROOT_INODE;
	}
	char filename[FILENAME_SIZE];
	GetFilenameFromPath(path, filename);
	char pathCopy[strlen(path)];
	strcpy(pathCopy, path);
	char *pathWithoutRoot = pathCopy + 1;
	if (strcmp(filename, pathWithoutRoot) == 0)
	{
		return getFileInodeNumber(filename, inodeNumber);
	}
	else
	{
		char *dirName;
		dirName = strtok(pathCopy, "/");
		int dirInodeNumber = getFileInodeNumber(dirName, inodeNumber);
		if (dirInodeNumber == -1)
		{
			return -1;
		}
		getInodeNumberFromPath(path + strlen(dirName) + 1, dirInodeNumber);
	}
}

void getInodegStat(const ino iNodeNumber, gstat *gstatCopy)
{
	int iNodeBlockNumber = getDirBlockNumber(iNodeNumber);
	char pBuffer[BLOCK_SIZE];
	ReadBlock(iNodeBlockNumber, pBuffer);
	iNodeEntry *fileInodeEntry = (iNodeEntry *)pBuffer;
	*gstatCopy = fileInodeEntry->iNodeStat;
}

// Pour faire des tests
void listAllChildren(int inodeNumber)
{
	int dirBlockNumber = getDirBlockNumber(inodeNumber);
	char blockBuffer[BLOCK_SIZE];
	ReadBlock(dirBlockNumber, blockBuffer);
	iNodeEntry *inodeEntry = (iNodeEntry *)blockBuffer;
	int numberOfDirEntry = inodeEntry->iNodeStat.st_size / 16;
	ReadBlock(inodeEntry->Block[0], blockBuffer);
	DirEntry *pDE = (DirEntry *)blockBuffer;
	for (int i = 0; i < numberOfDirEntry; i++)
	{
		printf("nom sub dir/file: %s\n", pDE[i].Filename);
	}
}


int changePermission(int inodeNumber, UINT16 new_mode)
{
	int iNodeBlockNumber = getDirBlockNumber(inodeNumber);
	char pBuffer[BLOCK_SIZE];
	ReadBlock(iNodeBlockNumber, pBuffer);
	iNodeEntry *fileInodeEntry = (iNodeEntry *)pBuffer;
	UINT16 mask1 = 3640; // 1111 0000 1111 0000
	UINT16 mask2 = 455; // 0000 1111 0000 1111
	fileInodeEntry->iNodeStat.st_mode = (fileInodeEntry->iNodeStat.st_mode & mask1) | (new_mode & mask2);
	WriteBlock(iNodeBlockNumber, pBuffer);
}

int bd_countfreeblocks(void)
{
	char BlockFreeMap[BLOCK_SIZE];
	ReadBlock(FREE_BLOCK_BITMAP, BlockFreeMap);
	int numberOfFreeBlocks = 0;
	for (int i = 0; i < N_BLOCK_ON_DISK; i++)
	{
		if (BlockFreeMap[i] != 0)
		{
			numberOfFreeBlocks += 1;
		}
	}
	return numberOfFreeBlocks;
}

int bd_stat(const char *pFilename, gstat *pStat)
{
	ino iNodeNumber = getInodeNumberFromPath(pFilename, ROOT_INODE);
	if (iNodeNumber == -1)
	{
		return -1;
	}
	getInodegStat(iNodeNumber, pStat);
	return 0;
}

int bd_chmod(const char *pFilename, UINT16 new_mode)
{
	ino iNodeNumber = getInodeNumberFromPath(pFilename, ROOT_INODE);
	if (iNodeNumber == -1)
	{
		return -1;
	}
	if (changePermission(iNodeNumber, new_mode) == -1)
	{
		return -1;
	}
	return 0;
}

int bd_readdir(const char *pDirLocation, DirEntry **ppListeFichiers)
{
	ino iNodeNumber = getInodeNumberFromPath(pDirLocation, ROOT_INODE);
	if (iNodeNumber == -1)
	{
		return -1;
	}
	int dirBlockNumber = getDirBlockNumber(iNodeNumber);
	char blockBuffer[BLOCK_SIZE];
	ReadBlock(dirBlockNumber, blockBuffer);
	iNodeEntry *inodeEntry = (iNodeEntry *)blockBuffer;
	if (inodeEntry->iNodeStat.st_mode & G_IFDIR)
	{
		int numberOfDirEntry = inodeEntry->iNodeStat.st_size / sizeof(DirEntry);
		ReadBlock(inodeEntry->Block[0], blockBuffer);
		(*ppListeFichiers) = (DirEntry *)malloc(sizeof(blockBuffer));
		DirEntry *pDE = (DirEntry *)blockBuffer;
		memcpy((*ppListeFichiers), blockBuffer, sizeof(blockBuffer));
		return numberOfDirEntry;
	}
	return -1;
}

int bd_hardlink(const char *pPathExistant, const char *pPathNouveauLien)
{
	ino pathExistantInodeNumber = getInodeNumberFromPath(pPathExistant, ROOT_INODE);
	if (pathExistantInodeNumber == -1)
	{
		return -1;
	}

	ino pathNouveauLienInodeNumber = getInodeNumberFromPath(pPathNouveauLien, ROOT_INODE);
	if (pathNouveauLienInodeNumber != -1)
	{
		return -2;
	}

	int pathExistantBlockNumber = getDirBlockNumber(pathExistantInodeNumber);
	char blockBuffer[BLOCK_SIZE];
	ReadBlock(pathExistantBlockNumber, blockBuffer);
	iNodeEntry *pathExistantInodeEntry = (iNodeEntry *)blockBuffer;
	if (pathExistantInodeEntry->iNodeStat.st_mode & G_IFDIR)
	{
		return -3;
	}

	char pathNouveauLienDirName[BLOCK_SIZE];
	GetDirFromPath(pPathNouveauLien, pathNouveauLienDirName);
	ino dirPathNouveauLienInodeNumber = getInodeNumberFromPath(pathNouveauLienDirName, ROOT_INODE);
	if (dirPathNouveauLienInodeNumber == -1)
	{
		return -1;
	}

	int pathNouveauLienDirectoryBlockNumber = getDirBlockNumber(dirPathNouveauLienInodeNumber);

	char newBlockBuffer[BLOCK_SIZE];
	ReadBlock(pathNouveauLienDirectoryBlockNumber, newBlockBuffer);
	iNodeEntry *pathNouveauLienInodeEntry = (iNodeEntry *)newBlockBuffer;

	char pathNouveauLienDataBuffer[BLOCK_SIZE];
	int numberOfDirEntry = pathNouveauLienInodeEntry->iNodeStat.st_size / sizeof(DirEntry);
	ReadBlock(pathNouveauLienInodeEntry->Block[0], pathNouveauLienDataBuffer);
	DirEntry *pDE = (DirEntry *)pathNouveauLienDataBuffer;
	pDE[numberOfDirEntry].iNode = pathExistantInodeNumber;
	char filename[FILENAME_SIZE];
	GetFilenameFromPath(pPathNouveauLien, filename);
	strcpy(pDE[numberOfDirEntry].Filename, filename);

	WriteBlock(pathNouveauLienInodeEntry->Block[0], pathNouveauLienDataBuffer);

	pathExistantInodeEntry->iNodeStat.st_nlink += 1;
	WriteBlock(pathExistantBlockNumber, blockBuffer);

	pathNouveauLienInodeEntry->iNodeStat.st_size += sizeof(DirEntry);
	WriteBlock(pathNouveauLienDirectoryBlockNumber, newBlockBuffer);

	return 0;
}

int bd_rename(const char *pFilename, const char *pDest)
{
	char pDestDir[BLOCK_SIZE];
	GetDirFromPath(pDest, pDestDir);

	int fileInodeNumber = getInodeNumberFromPath(pFilename, ROOT_INODE);
	int destInodeNumber = getInodeNumberFromPath(pDestDir, ROOT_INODE);

	if (fileInodeNumber == -1 || destInodeNumber == -1)
	{
		return -1;
	}

	bd_hardlink(pFilename, pDest);
	bd_unlink(pFilename);

	return 0;
}

int getFreeInode() {
	//get # d'une inode vide
	char bitmap[BLOCK_SIZE];
	ReadBlock(FREE_BLOCK_BITMAP, bitmap);
	for (int i = ROOT_INODE; i < N_INODE_ON_DISK; i++) {
		if (bitmap[i] == 1) {
			bitmap[i] = 0;
			return i;
		}
	}
}

int bd_mkdir(const char *pDirName)
{

	//get # d'une inode vide
	int inodeNumber = getFreeInode();

	//get la inode
	int iNodeBlockNumber = getDirBlockNumber(inodeNumber);
	char pBuffer[BLOCK_SIZE];
	ReadBlock(iNodeBlockNumber, pBuffer);
	iNodeEntry *dirInodeEntry = (iNodeEntry *)pBuffer;

	//changer ses attributs pour dire que c'est un repertoire
	dirInodeEntry->iNodeStat.st_ino = inodeNumber;
    dirInodeEntry->iNodeStat.st_mode = G_IFDIR;
	dirInodeEntry->iNodeStat.st_size = 2 * sizeof(DirEntry);
	dirInodeEntry->iNodeStat.st_blocks = 1;
    
	//lien . et ..
	dirInodeEntry->iNodeStat.st_nlink = 2;
	
	WriteBlock(iNodeBlockNumber, pBuffer);

	//linker son parent à lui (directory de pdirname)
	//get parent inode
	char pathParent[256];
	GetDirFromPath(pDirName, pathParent);
	ino pathParentInodeNumber = getInodeNumberFromPath(pathParent, ROOT_INODE);
	int pathParentBlockNumber = getDirBlockNumber(pathParentInodeNumber);
	char newBlockBuffer[BLOCK_SIZE];
	ReadBlock(pathParentBlockNumber, newBlockBuffer);
	iNodeEntry *pathParentInodeEntry = (iNodeEntry *)newBlockBuffer;

	//Ajouter le directory dans la inode du parent
	char directoryInfo[BLOCK_SIZE];
	int numberOfDirEntry = pathParentInodeEntry->iNodeStat.st_size / sizeof(DirEntry);
	ReadBlock(pathParentInodeEntry->Block[0], directoryInfo);
	DirEntry *pDE = (DirEntry *)directoryInfo;
	pDE[numberOfDirEntry].iNode = inodeNumber;
	char filename[FILENAME_SIZE];
	strcpy(pDE[numberOfDirEntry].Filename, filename);
	pathParentInodeEntry->iNodeStat.st_nlink += 1;
	pathParentInodeEntry->iNodeStat.st_size += sizeof(DirEntry);
	WriteBlock(pathParentInodeEntry->Block[0], directoryInfo);

	return 1;
}

int bd_create(const char *pFilename)
{
	//get # d'une inode vide
	int inodeNumber = getFreeInode();

	//get la inode
	int iNodeBlockNumber = getDirBlockNumber(inodeNumber);
	char pBuffer[BLOCK_SIZE];
	ReadBlock(iNodeBlockNumber, pBuffer);
	iNodeEntry *dirInodeEntry = (iNodeEntry *)pBuffer;

	//changer ses attributs pour dire que c'est un fichier
	dirInodeEntry->iNodeStat.st_ino = inodeNumber;
    dirInodeEntry->iNodeStat.st_mode = G_IFREG;
	dirInodeEntry->iNodeStat.st_size = 0; 
	dirInodeEntry->iNodeStat.st_blocks = 1;
    
	//lien . et ..
	dirInodeEntry->iNodeStat.st_nlink = 2;

	WriteBlock(iNodeBlockNumber, pBuffer);

	//linker son parent à lui (directory de pfilename)
	//get parent inode
	char pathParent[256];
	GetDirFromPath(pFilename, pathParent);
	ino pathParentInodeNumber = getInodeNumberFromPath(pathParent, ROOT_INODE);
	int pathParentBlockNumber = getDirBlockNumber(pathParentInodeNumber);
	char newBlockBuffer[BLOCK_SIZE];
	ReadBlock(pathParentBlockNumber, newBlockBuffer);
	iNodeEntry *pathParentInodeEntry = (iNodeEntry *)newBlockBuffer;

	//Ajouter le fichier dans la inode du parent
	char directoryInfo[BLOCK_SIZE];
	int numberOfDirEntry = pathParentInodeEntry->iNodeStat.st_size / sizeof(DirEntry);
	ReadBlock(pathParentInodeEntry->Block[0], directoryInfo);
	DirEntry *pDE = (DirEntry *)directoryInfo;
	pDE[numberOfDirEntry].iNode = inodeNumber;
	char filename[FILENAME_SIZE];
	strcpy(pDE[numberOfDirEntry].Filename, filename);
	pathParentInodeEntry->iNodeStat.st_nlink += 1;
	pathParentInodeEntry->iNodeStat.st_size += sizeof(DirEntry);
	WriteBlock(pathParentInodeEntry->Block[0], directoryInfo);

	return 1;
}

int bd_rmdir(const char *pDirName)
{
	int destInodeNumber = getInodeNumberFromPath(pDirName, ROOT_INODE);

	if (destInodeNumber == -1)
	{
		return -1;
	}

	int destBlockNumber = getDirBlockNumber(destInodeNumber);
	char pDirBuffer[BLOCK_SIZE];
	ReadBlock(destBlockNumber, pDirBuffer);
	iNodeEntry *destDirInodeEntry = (iNodeEntry *)pDirBuffer;
	if (destDirInodeEntry->iNodeStat.st_size / sizeof(DirEntry) != 2)
	{
		return -3;
	}

	bd_unlink2(pDirName);
}

int bd_unlink(const char *pFilename)
{
	int returnValue = 1;
	int inodeNumber = getInodeNumberFromPath(pFilename, ROOT_INODE);
	if (inodeNumber == -1)
	{
		return -1;
	}
	// Get inode
	int iNodeBlockNumber = getDirBlockNumber(inodeNumber);
	char pBuffer[BLOCK_SIZE];
	ReadBlock(iNodeBlockNumber, pBuffer);
	iNodeEntry *fileInodeEntry = (iNodeEntry *)pBuffer;

	char filename[FILENAME_MAX];
	GetFilenameFromPath(pFilename, filename);

	if (!(fileInodeEntry->iNodeStat.st_mode & G_IFREG))
	{
		return -2;
	}

	// decrementer le nlink
	fileInodeEntry->iNodeStat.st_nlink--;

	// release le bloque
	// il y a un autre hardlink
	if (fileInodeEntry->iNodeStat.st_nlink == 0)
	{
		ReleaseFreeBlock(iNodeBlockNumber);
		returnValue = 0;
	}

	WriteBlock(iNodeBlockNumber, pBuffer);

	// get parent
	char parentDir[BLOCK_SIZE];
	GetDirFromPath(pFilename, parentDir);
	int parentDirInodeNumber = getInodeNumberFromPath(parentDir, ROOT_INODE);
	int parentDirBlockNumber = getDirBlockNumber(parentDirInodeNumber);
	char pDirBuffer[BLOCK_SIZE];
	ReadBlock(parentDirBlockNumber, pDirBuffer);
	iNodeEntry *parentDirInodeEntry = (iNodeEntry *)pDirBuffer;
	// remove parent link
	int numberOfDirEntry = parentDirInodeEntry->iNodeStat.st_size / sizeof(DirEntry);

	char blockBuffer[BLOCK_SIZE];
	ReadBlock(parentDirInodeEntry->Block[0], blockBuffer);
	DirEntry *parentDirEntry = (DirEntry *)blockBuffer;

	int foundBlockToDelete = 0;
	for (int i = 0; i < numberOfDirEntry; i++)
	{
		if (foundBlockToDelete == 1)
		{
			parentDirEntry[i - 1] = parentDirEntry[i];
		}

		if (strcmp(parentDirEntry[i].Filename, filename) == 0)
		{
			foundBlockToDelete = 1;
		}
	}

	parentDirInodeEntry->iNodeStat.st_size -= sizeof(DirEntry);
	WriteBlock(parentDirInodeEntry->Block[0], blockBuffer);
	WriteBlock(parentDirBlockNumber, pDirBuffer);

	return returnValue;
}

int bd_unlink2(const char *pFilename)
{
	int returnValue = 1;
	int inodeNumber = getInodeNumberFromPath(pFilename, ROOT_INODE);
	if (inodeNumber == -1)
	{
		return -1;
	}
	// Get inode
	int iNodeBlockNumber = getDirBlockNumber(inodeNumber);
	char pBuffer[BLOCK_SIZE];
	ReadBlock(iNodeBlockNumber, pBuffer);
	iNodeEntry *fileInodeEntry = (iNodeEntry *)pBuffer;

	char filename[FILENAME_MAX];
	GetFilenameFromPath(pFilename, filename);

	if (!(fileInodeEntry->iNodeStat.st_mode & G_IFDIR))
	{
		return -2;
	}

	// decrementer le nlink
	fileInodeEntry->iNodeStat.st_nlink--;

	// release le bloque
	// il y a un autre hardlink
	if (fileInodeEntry->iNodeStat.st_nlink == 0)
	{
		ReleaseFreeBlock(iNodeBlockNumber);
		returnValue = 0;
	}

	WriteBlock(iNodeBlockNumber, pBuffer);

	// get parent
	char parentDir[BLOCK_SIZE];
	GetDirFromPath(pFilename, parentDir);
	int parentDirInodeNumber = getInodeNumberFromPath(parentDir, ROOT_INODE);
	int parentDirBlockNumber = getDirBlockNumber(parentDirInodeNumber);
	char pDirBuffer[BLOCK_SIZE];
	ReadBlock(parentDirBlockNumber, pDirBuffer);
	iNodeEntry *parentDirInodeEntry = (iNodeEntry *)pDirBuffer;
	// remove parent link
	int numberOfDirEntry = parentDirInodeEntry->iNodeStat.st_size / sizeof(DirEntry);

	char blockBuffer[BLOCK_SIZE];
	ReadBlock(parentDirInodeEntry->Block[0], blockBuffer);
	DirEntry *parentDirEntry = (DirEntry *)blockBuffer;

	int foundBlockToDelete = 0;
	for (int i = 0; i < numberOfDirEntry; i++)
	{
		if (foundBlockToDelete == 1)
		{
			parentDirEntry[i - 1] = parentDirEntry[i];
		}

		if (strcmp(parentDirEntry[i].Filename, filename) == 0)
		{
			foundBlockToDelete = 1;
		}
	}

	parentDirInodeEntry->iNodeStat.st_size -= sizeof(DirEntry);
	WriteBlock(parentDirInodeEntry->Block[0], blockBuffer);
	WriteBlock(parentDirBlockNumber, pDirBuffer);

	return returnValue;
}
